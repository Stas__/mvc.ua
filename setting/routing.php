<?php

$routes = 
[
    'home' => 
    [
        'controller'=> 'HomeController',
        'path' => 'controller/HomeController.php',
        'action'=> 'index',
        //'tpl' =>'',
        
    ],
    'page' => 
    [
         'controller'=> 'PageController',
        'path' => 'controller/PageController.php',
        //'params' => ['slug', 'num'],
        'action'=> 'index',
        'slug' => [
            'reg' => '/^[0-9]{1,10}$/',
            'default' => '1'
                  ],
    ],
    'pages' => 
    [
         'controller'=> 'PageController',
        'path' => 'controller/PageController.php',
        'action'=> 'getPages',
    ],
    'register' => 
    [
         'controller'=> 'RegisterController',
        'path' => 'controller/RegisterController.php',
        'action'=> 'index',
    ],
    'login' => 
    [
         'controller'=> 'LoginController',
        'path' => 'controller/LoginController.php',
        'action'=> 'index',
    ],
    'register-save' => 
    [
         'controller'=> 'RegisterController',
        'path' => 'controller/RegisterController.php',
        'action'=> 'save',
    ]
];