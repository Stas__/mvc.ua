<?php
include './model/HeaderModel.php';


class HeaderController extends Controller 
{
    public function index()
    {
        $Model = new HeaderModel();
        $user_login='';
        if($_SESSION['user']){
            $user_login = $_SESSION['user']['login'];
        }
        
        $pages = $Model->getLinkPages();
        
        include ('./view/header.php');
    }
}
