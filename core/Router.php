<?php


class Router {
    /**
     *
     * @var string
     */
    private $route;
        /**
     *
     * @var arrey*/
    private $routes;
    /**
     * Конструктор
     */
     public function __construct()
             {
                 $this->route = $_GET['route'];
                        /**
                        * подключаем массив с роутами 
                        */
                 include './setting/routing.php';
                         // присваиваем массив с роутами в локальную переменую
                 $this->routes = $routes;
                         // присваеваем строку запроса пользователя в локал переменую
                 $this->route = $_GET['route'];
             }
             /**
              * роутер
              */
    public function route()
            {

                 /**
                  * если роут пустой то подключаем контроллер домашней страницы
                  */
                 if($this->route === NULL)
                     {
                        include './controller/HomeController.php';
                         $Home = new HomeController();
                         $Home->index();
                     }
                 /**
                  * если есть роут(путь) то разбираем его по слешу
                  */
                 if ($this->route) 
                     {
                     // разбираем url по слешу    
                     $dataController = $this->getPatch();
                    
                     //проверяем если в массиве с роутами данный контроллер
                     if(isset($this->routes[$dataController['name']]))
                         {
                         // подключаем контролер    
                         $this->showController($dataController);                        
                            
                         }
                    else {
                          $this->getError(404);
                         }

                  
                    }   

            }
    private function showController($data)
            {

                  //var_dump($data);        die();
        
                $route= $this->routes[$data['name']];
        
                // подключаем нужный контроллер 
                  include $route['path'];
                            
                // создаем экзепляр класса контроллера "обьект"
                   $Controller = new $route['controller']();

                // вызываем метод контроллера
                   $Action = $route['action'];

                // проверяем на наличие доп параметра в запросе
                if(isset($route['slug']) && isset($data['slug']))
               {
                    $Controller->$Action($data['slug']);
               }
                elseif (isset($route['slug']) && !isset($data['slug'])) 
                    {
                     $Controller->$Action(NULL);
                    }
                else
                    {
                          $Controller->$Action();
                      } 


            }

    /**
     * метод для разобра данных из запроса пользователся
     * 
     * @return array
     */
    private function getPatch()
            {
            $result =[];
                     // разбираем роут послешу
                     $pach_routes = explode('/', $this->route);
                    
                     //получаем название контролера
                   $result['name'] = $pach_routes[0];
                   //получаем параметры контроллера 
                  
                   if (isset($pach_routes[1]))
                       {
                       $result['slug']=$pach_routes[1];
                       }
            return $result;
            }

    private function getError($er)
            {
         // если роутер не валидный то выдать 404 страницу
                     include './controller/ErrorsController.php';
                     $Errors = new ErrorsController();
                     $Errors->getError($er);
            }
}

