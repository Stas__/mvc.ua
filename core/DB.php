<?php

class DB 
{
    private $dbh;
    private static $instants = Null;
    public $test;
    
    public function __construct() 
    {
        include './setting/db_params.php';
        try
        {
        $this->dbh = new PDO("mysql:host={$params['host']}; dbname={$params['name']}; charset=utf8", $params['user'], $params['pass']);

        } 
        catch (PDOException $ex) 
        {
            echo $ex->getMessage();
        }

    }
    
    public function query($sql, $type = true) 
    {
        $result = $this->dbh->query($sql);
        if($type)
        {
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }
        else
        {
            return $result->fetchAll(PDO::FETCH_NUM);
        }
    }
    public static function getInstants ()
    {
        if(!self::$instants)
        {
            self::$instants = new self();
        }
        return self::$instants;
    }
        public function SinglQuery($sql, $type = true) 
    {
        $result = $this->dbh->query($sql);
        if($type)
        {
            return $result->fetch(PDO::FETCH_ASSOC);
        }
        else
        {
            return $result->fetch(PDO::FETCH_NUM);
        }
    }
}
