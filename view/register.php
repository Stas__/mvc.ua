<div id="registretion">
<style>
   body{
   background: url(images/fones.jpg);
   }
</style>
        <h1>Регистрация</h1>
        <form>
            <div class="row">
                <label for="login">Login</label>
                <input type="text" id="login" />
                <span id="login_er" class="hide">Не валидный login</span>
            </div>
            <div class="row">
                <label for="email">Email</label>
                <input type="email" id="email" />
                <span id="email_er" class="hide">Не валидный email</span>
            </div>
            <div class="row">
                <label for="password">Password</label>
                <input type="password" id="password" />
                <span id="pass_er" class="hide">Не валидный пароль</span>
            </div>
            <button id='send' type="button">Отправить</button>
        </form>
</div>

<script>
function sendRegister(){
    var login = document.getElementById('login');
    var email = document.getElementById('email');
    var pass = document.getElementById('password');
    
    var er_login = document.getElementById('login_er');
    var er_email = document.getElementById('email_er');
    var er_pass = document.getElementById('pass_er');
        console.log(er_email.className);
    var data = {
        login: login.value, 
        email: email.value,
        pass: pass.value
    };
/*
 * отправка методом fetch
 */
    fetch('/register-save', {
        method:'POST',
        body:JSON.stringify(data)
    })
    .then(function(response){
        return response.json();
    })
    .then(function(data){
        console.log(data);

        er_login.className = er_email.className = er_pass.className = 'hide';

        if(data['login']){
            er_login.className = '';
        }
        if(data['email']){
            er_email.className = '';
        }
        if(data['pass']){
            er_pass.className = '';
        }
    })
    .catch(console.log);
    

}
var send = document.getElementById('send');

send.onclick = sendRegister;
</script>